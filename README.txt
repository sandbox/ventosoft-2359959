-- SUMMARY --

The RedHelper module attaches a redhelper.ru live chat to your web site.
This chat allows visitors to communicate to site support/sale team in a new way.
The site team may monitor visitors' activity and even start dialogs.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

Go to admin/config/redhelper and enter your RedHelper login, if you don't have it, you can register here: admin/config/redhelper/register