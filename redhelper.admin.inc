<?php

define('REDHELPER_REGISTER_URL', 'http://redhelper.ru/my/register');

function redhelper_config_form($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'redhelper') . '/redhelper.css', 'file');

  $form['#prefix'] = "<div id='redhelper-logo'></div>";

  $form['redhelper_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Redhelper Chat Module Login'),
    '#default_value' => variable_get('redhelper_login'),
    '#description' => t('Enter login'),
  );

  $form['register'] = array(
    '#type' => 'markup',
    '#markup' => t('If you don\'t have a RedHelper login you can register <a href="!url">here</a>.', array('!url' => url('admin/config/user-interface/redhelper/register'))),
  );
  
  return system_settings_form($form);
}

function redhelper_register_form($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'redhelper') . '/redhelper.css', 'file');

  $form['#prefix'] = "<div id='redhelper-logo'></div>";
  
  $form['register'] = array(
    '#type' => 'markup',
    '#markup' => t('If you don\'t have a RedHelper login you can register here'),
  );
  
  $form['login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#description' => t('Choose your unique login. Login should be at least 3 latin letters or digits'),
    '#required' => true,
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#description' => t('Enter your contact e-mail address.'),
    '#required' => true,
  );
  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Choose your password in RedHelper, it should differ from InSales password'),
    '#required' => true,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Your name'),
    '#required' => true,
  );
  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#description' => t('Your contact phone, please enter only digits with city and country prefix'),
    '#required' => true,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register'),
  );
  return $form;
}

function redhelper_register_form_validate($form, &$form_state) {
  if(mb_strlen($form_state['values']['login']) < 3 || !preg_match("/^[A-Za-z0-9]+$/", $form_state['values']['login'])) {
    form_set_error('login', t('Login should be at least 3 latin letters or digits'));
  }
  if(mb_strlen($form_state['values']['phone']) < 10 || !preg_match("/^[0-9]+$/", $form_state['values']['phone'])) {
    form_set_error('phone', t('Phone should be at least 10 digits'));
  }
}
function redhelper_register_form_submit($form, &$form_state) {
  //$result = "success";
  $result = _redhelper_register($form_state['values']['login'], $form_state['values']['email'], $form_state['values']['password'], $form_state['values']['name'], $form_state['values']['phone']);
  if(mb_stripos($result, "success") !== false) {
    drupal_set_message("Congratulations! You have installed RedHelper online-chat! Please visit <a href='" . url('admin/config/user-interface/redhelper/application') . "'>this page</a> for further instructions.");
    variable_set('redhelper_login', $form_state['values']['login']);
  } else {
    if($result == "exist") {
      form_set_error('login', t('This login is already registered, please choose another one.'));
    }
    if($result == "E-mail incorrect") {
      form_set_error('email', t('E-mail is incorrect'));
    }
    $form_state['rebuild'] = TRUE;
  }
}

function _redhelper_register($login, $email, $password, $name, $phone) {
  global $language;
  $params = array(
    'login' => $login,
    'email' => $email,
    'password' => $password,
    'contactfio' => $name,
    'contactphone' => $phone,
    'locale' => $language->language,
    'ref' => 2003418
  );
  $filename = REDHELPER_REGISTER_URL . "?" . http_build_query($params);
  $result = @file_get_contents($filename);
  watchdog('redhelper', $filename . ": " . $result);
  return $result;
}

function redhelper_application() {
  drupal_add_css(drupal_get_path('module', 'redhelper') . '/redhelper.css', 'file');

  $output = <<<HEREDOC
<p>You need to install application for operator and configure the system in your <a href='http://redhelper.ru/my/login' target=_blank>RedHelper account</a></p>
<h2>1. Application installation</h2>
<p>You need to download and install application for operator. It lets you to watch your visitors, answer their questions and many other things. Please download the application for Windows or Mac OS with the links below. For other operation systems you can use any Jabber client (<a href='https://redhelper.zendesk.com/forums/21556502-jabber-redhelper' target=_blank>How?</a>)</p>
<a href='http://update.redhelper.ru/RedHelperSetup.ru-RU.exe' id='redhelper-download-windows'></a> <a href='http://update.redhelper.ru/RedHelper.dmg' id='redhelper-download-macos'></a>
<p>После установки используйте логин и пароль, выбранный при регистрации, для входа в приложение. Для завершения установки, попробуйте пообщаться через приложение. Для этого зайдите на сайт (обновите страницу), нажмите на значок "Задать вопрос" и напишите пару фраз в открывшемся окне. Сообщения придут в приложение оператора.</p>
<h2>2. Configuring RedHelper</h2>
<p>Всё готово для использования RedHelper на вашем сайте, но для максимальной эффективности работы системы необходимо произвести настройку внешнего вида и поведения RedHelper для вашего сайта.</p>
<a href='http://redhelper.ru/my/login' target=_blank id='redhelper-account'>Перейти в Личный кабинет RedHelper</a>
<p class='comment'>Используйте в дальнейшем для перехода в Личный кабинет наш сайт  <a href='http://redhelper.ru/'>http://redhelper.ru/</a></p>
<p></p>
<p>Если Вам требуется помощь, напишите или позвоните нам.</p>
<p><b>Отдел технической поддержки клиентов RedHelper.</b></p>
<p>+7 (495) 221-77-57,  (доб. 201)</p>
<p><a href='mailto:info@redhelper.ru'>info@redhelper.ru</a></p>

<p>База знаний центра технической поддержки RedHelper</p>

<p>Вы так же можете получить поддержку в режиме онлайн на нашем сайте: <a href='http://redhelper.ru/'>http://redhelper.ru/</a></p>
HEREDOC;
  return $output;
}